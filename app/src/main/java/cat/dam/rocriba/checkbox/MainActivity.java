package cat.dam.rocriba.checkbox;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    RadioGroup rb_selecio;
    TextView textr;
    LinearLayout linearLayout;
    ConstraintLayout main;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //variables principals
        rb_selecio = (RadioGroup) findViewById(R.id.rg_base);
        textr = (TextView) findViewById(R.id.textView1);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout2);
        main = (ConstraintLayout) findViewById(R.id.clmain);

        //variables RadioGrups
        RadioGroup text, cll, cfll, cf;

        text = (RadioGroup) findViewById(R.id.rg_text);
        cll = (RadioGroup) findViewById(R.id.rb_clletra);
        cfll = (RadioGroup) findViewById(R.id.rb_cflletra);
        cf = (RadioGroup) findViewById(R.id.rb_cfons);
        //Per defecte els posem tots en out (menys text)
        cll.setVisibility(View.GONE);
        cfll.setVisibility(View.GONE);
        cf.setVisibility(View.GONE);
        text.setVisibility(View.GONE);

        //variables color lletra
        EditText cllt;
        cllt = (EditText) findViewById(R.id.et_2);

        //Variables color background
        EditText edcf;
        edcf = (EditText) findViewById(R.id.et_3);
        //Variables color fons lletra
        EditText edcfll;
        edcfll = (EditText) findViewById(R.id.et_4);

        //Variables RadioButton
        RadioButton cfp1, cfp2, cfp3, cfp4, cfp5;

        cfp1 = (RadioButton) findViewById(R.id.rb_cfl);
        cfp2 = (RadioButton) findViewById(R.id.rb_cfl2);
        cfp3 = (RadioButton) findViewById(R.id.rb_cfl3);
        cfp4 = (RadioButton) findViewById(R.id.rb_cfl4);
        cfp5 = (RadioButton) findViewById(R.id.rb_cfl5);



        //Fem un mètode amb un if, comprovem quin boto està cheked, i segons quin ho estigui
        //fem uns elements gone o uns alres. Dintre del mateix if continua el programa
        rb_selecio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged (RadioGroup group, int checkedId) {
                //quin botó s'ha selecionat?
                if (checkedId == R.id.rb_lletra){
                    //Posar elements a GONE
                    cll.setVisibility(View.GONE);
                   cfll.setVisibility(View.GONE);
                    cf.setVisibility(View.GONE);
                    text.setVisibility(View.VISIBLE);
                    //per cada ID fer una opció
                    text.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            //Busquem quin id s'ha selecionat i apliquem el textsize segons el botó selecionat
                            if (checkedId == R.id.rb_text1) textr.setTextSize(10);
                            else if (checkedId == R.id.rb_text2) textr.setTextSize(15);
                            else if (checkedId == R.id.rb_text3) textr.setTextSize(20);
                            else if (checkedId == R.id.rb_text4) textr.setTextSize(33);
                            else if (checkedId == R.id.rb_text5) textr.setTextSize(45);
                            else textr.setText("Error");
                        }
                    });



                } else if (checkedId == R.id.rb_color){
                    text.setVisibility(View.GONE);
                    cll.setVisibility(View.VISIBLE);
                    cfll.setVisibility(View.VISIBLE);
                    cf.setVisibility(View.VISIBLE);
                    //En cas de que clilqui color
                    //Configurem el color del text
                    cll.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.rb_ll1) textr.setTextColor(Color.RED);
                            else if (checkedId == R.id.rb_ll2) textr.setTextColor(Color.GREEN);
                            else if (checkedId == R.id.rb_ll3) textr.setTextColor(Color.BLUE);
                            else if (checkedId == R.id.rb_ll4) textr.setTextColor(Color.WHITE);
                            else if (checkedId == R.id.rb_ll5) textr.setTextColor(Color.BLACK);
                            else if (checkedId == R.id.et_2) {
                                cllt.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        cllt.setInputType(InputType.TYPE_CLASS_TEXT);
                                        String et2 = cllt.getText().toString();
                                        et2.toLowerCase();
                                        if (et2.length() > 6 || et2.startsWith("#")){
                                            textr.setText("Error entrada HEX");
                                        } else {
                                            textr.setTextColor(Color.parseColor("#" + et2));
                                        }
                                    }
                                });

                            }
                        }
                    });

                    //Configurem color fons lletra

                    cfll.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.rb_cfl) linearLayout.setBackgroundColor(Color.RED);
                            else if (checkedId == R.id.rb_cfl2) linearLayout.setBackgroundColor(Color.GREEN);
                            else if (checkedId == R.id.rb_cfl3) linearLayout.setBackgroundColor(Color.BLUE);
                            else if (checkedId == R.id.rb_cfl4) linearLayout.setBackgroundColor(Color.WHITE);
                            else if (checkedId == R.id.rb_cfl5) linearLayout.setBackgroundColor(Color.BLACK);
                            else if (checkedId == R.id.et_4) {
                                edcfll.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        edcfll.setInputType(InputType.TYPE_CLASS_TEXT);
                                        String et3 = edcfll.getText().toString();
                                        et3.toLowerCase();
                                        if (et3.length() > 6 || et3.startsWith("#")){
                                            textr.setText("Error entrada HEX");
                                        } else {
                                            linearLayout.setBackgroundColor(Color.parseColor("#" + et3));
                                        }
                                    }
                                });

                            }
                        }
                    });

                    //Configurem color fons d'escriptori
                    cf.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.rb_cf1) main.setBackgroundColor(Color.RED);
                            else if (checkedId == R.id.rb_cf2) main.setBackgroundColor(Color.GREEN);
                            else if (checkedId == R.id.rb_cf3) main.setBackgroundColor(Color.BLUE);
                            else if (checkedId == R.id.rb_cf4) main.setBackgroundColor(Color.WHITE);
                            else if (checkedId == R.id.rb_cf5) main.setBackgroundColor(Color.BLACK);
                            else if (checkedId == R.id.et_3) {

                                edcf.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        edcf.setInputType(InputType.TYPE_CLASS_TEXT);
                                        String et4 = edcf.getText().toString();
                                        et4.toLowerCase();
                                        if (et4.length() > 6 || et4.startsWith("#")){
                                            textr.setText("Error entrada HEX");
                                        } else {
                                            main.setBackgroundColor(Color.parseColor("#" + et4));
                                        }
                                    }
                                });

                            }
                        }
                    });

                }else{
                    textr.setText("Error");
                }
            }
        });


    }




}